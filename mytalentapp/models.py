from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

class UserProfile(models.Model):
	user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
	email = models.CharField(max_length=100, default='')
	first_name = models.CharField(max_length=100, default='')
	last_name = models.CharField(max_length=100, default='')
	profession = models.CharField(max_length=100, default='')

	def __str__(self):
		return self.user.username

class Status(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	author = models.CharField(max_length=100, default='')
	text = models.TextField(max_length=500)
	created = models.DateTimeField(null=True, default=None)
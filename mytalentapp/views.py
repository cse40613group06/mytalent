from datetime import datetime

from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, TemplateView
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView

from django.contrib.auth.models import User
from .models import Status, UserProfile

class HomeView(ListView):
	model = Status
	template_name = 'home.html'

	def get_queryset(self):
		return Status.objects.all().order_by('-created')

class StatusView(CreateView):
	model = Status
	template_name = 'status.html'
	fields = ['text']

	def form_valid(self, form):
		form.instance.user = self.request.user
		form.instance.author = self.request.user.username
		form.instance.created = datetime.now()
		form.save()

		return redirect('home')

class SignUpView(CreateView):
	template_name = 'signup.html'
	form_class = UserCreationForm
	model = User

	def form_valid(self, form):
		form.save()
		username = form.cleaned_data.get('username')
		raw_password = form.cleaned_data.get('password1')
		user = authenticate(username=username, password=raw_password)

		profile = UserProfile.objects.create(user=user)

		return redirect('home')

class MyTalentLoginView(LoginView):
	template_name = 'login.html'

	def get_success_url(self):
		return self.request.GET.get('next', "/")

class MyTalentLogoutView(LogoutView):
	def get_next_page(self):
		return self.request.GET.get('next', "/")

class UserProfileView(LoginRequiredMixin, DetailView):
	model = UserProfile
	template_name = 'user_profile.html'

class UserProfileEditView(LoginRequiredMixin, UpdateView):
	model = UserProfile
	template_name  = 'user_profile_edit.html'
	fields = ['first_name', 'last_name', 'email', 'profession']

	def get_object(self):
		return UserProfile.objects.get(user=self.request.user)

	def form_valid(self, form):
		self.object = form.save()

		success_url = "/accounts/profile/" + str(self.request.user.id)

		return redirect(success_url)
from django.apps import AppConfig


class MytalentappConfig(AppConfig):
    name = 'mytalentapp'

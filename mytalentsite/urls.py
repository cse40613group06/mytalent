"""mytalentsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from mytalentapp import views

urlpatterns = [
	url(r'^admin/', admin.site.urls),

	url(r'^$', views.HomeView.as_view(), name="home"),

	url(r'^accounts/login/$', views.MyTalentLoginView.as_view(), name="login"),
	url(r'^accounts/logout/', views.MyTalentLogoutView.as_view(next_page="/"), name="logout"),
	url(r'^accounts/signup/$', views.SignUpView.as_view(), name="signup"),

	url(r'^accounts/profile/(?P<pk>\d+)$', views.UserProfileView.as_view(), name="user_profile"),
	url(r'^accounts/profile/(?P<pk>\d+)/edit/$', views.UserProfileEditView.as_view(), name="user_profile_edit"),

	url(r'^status/$', views.StatusView.as_view(), name="status"),
]
